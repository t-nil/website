+++
title = "Documentação"
+++

## Introdução

Nós recomendamos que você comece pelo [Livro](https://doc.redox-os.org/book/), pois ele descreve como configurar e usar o Redox.

## Referências

- [Livro](https://doc.redox-os.org/book/). Um livro que descreve o design do Redox.

- [redox_syscall](https://docs.rs/redox_syscall/latest/syscall/). Documentação para as chamadas de sistema do Redox.

- [libstd](https://doc.rust-lang.org/stable/std/). Documentação para a biblioteca padrão da Rust.

- [Manual da Ion](https://doc.redox-os.org/ion-manual/). Documentação para a shell Ion.

- [Palestras](/talks/). Palestras sobre o Redox em diversos eventos e conferências.

## Contribuindo pro Redox

- Leia o [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
