+++
title = "Documentation"
+++

## Getting Started

We highly recommend that you start with the [Book](https://doc.redox-os.org/book/), as it describes how to set up and use Redox.

## References

- [Book](https://doc.redox-os.org/book/). A Book describing Redox's design.

- [redox_syscall](https://docs.rs/redox_syscall/latest/syscall/). Documentation for the Redox system calls.

- [libstd](https://doc.rust-lang.org/stable/std/). Documentation for the Rust standard library.

- [Ion Manual](https://doc.redox-os.org/ion-manual/). Documentation for the Ion shell.

- [Talks](/talks/). Redox talks given at various events and conferences.

## Contributing to Redox

- Read the [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
