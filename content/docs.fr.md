+++
title = "Documentation"
+++

## Débuter

Il est fortement recommandé de commencer par le
[livre](https://doc.redox-os.org/book/), qui explique comment configurer
et utiliser Redox.

## Références

- [Livre](https://doc.redox-os.org/book/). Un livre présentant l'architecture de Redox.

- [redox_syscall](https://docs.rs/redox_syscall/latest/syscall/). Documentation for the Redox system calls.

- [libstd](https://doc.rust-lang.org/stable/std/). Documentation de la bibliothèque standard de Rust.

- [Manuel de Ion](https://doc.redox-os.org/ion-manual/). Documentation du shell de Ion.

- [Conférences](/talks/). Conférences de Redox réalisées à différents événements.

## Contribuer à Redox

- Lisez [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
