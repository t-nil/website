+++
title = "Comunidade"
+++

Essa página explica como a comunidade do Redox OS é organizada e como você pode acessar ela.

Nós seguidos o [Código de Conduta da Rust](https://www.rust-lang.org/policies/code-of-conduct) como regra em todos os nossos canais de comunidade/chats.

## [Chat](https://matrix.to/#/#redox:matrix.org)

Matrix é a forma de comunicação oficial com o time/comunidade do Redox OS (aceitamos apenas o Inglês nestas salas, pois não entendemos outras linguagens).

- #redox:matrix.org

A rede Matrix tem diferentes clientes, [Element](https://element.io/) é o mais utilizado.

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

Uma forma mais formal de comunicação com os desenvolvedores do Redox, porém mais lento e menos conveniente de conversar.

Envie uma Issue se você tiver problemas compilando/testando ou apenas queira discutir algum assunto, seja funções, estilo de código, inconsistências de código, pequenas mudanças ou correções.

## [Reddit](https://www.reddit.com/r/Redox/)

Caso queira ver as novidades e discutir sobre.

[reddit.com/r/rust](https://www.reddit.com/r/rust) - Para notícias relacionadas a Rust e discussões.

## [Mastodon](https://fosstodon.org/@redox)

Notícias e conteúdo relacionado.

## [Mattermost](https://chat.redox-os.org)

Até recentemente, estavamos utilizando o servidor de chat Mattermost, o qual nós estamos hospedando para consulta histórica.

Nos chame em "Redox OS/General" se você precisar acessar o servidor Mattermost.

## [Fórum](https://discourse.redox-os.org/)

This is our historical forum with old/classic questions, it's inactive and must be used for historical purposes, if you have a question, send on Matrix.
Nosso fórum histórico com perguntas clássicas/antigas, está inátivo e deve ser usado para consulta histórica.

Se você tem uma pergunta, faça no chat Matrix.

## [Palestras](/talks/)

Palestras sobre o Redox em diversos eventos e conferências.

### Nota

A divulgação da comunidade é uma parte importante no sucesso do Redox, quanto mais pessoas souberem sobre o Redox, mais contribuições podem vir e todos podem se beneficiar.

Você pode fazer a diferença escrevendo artigos, conversando com entusiastas de sistemas operacionais ou procurando por comunidades que podem estar interessadas em conhecer o Redox.
